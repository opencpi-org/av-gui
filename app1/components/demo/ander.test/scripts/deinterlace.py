from __future__ import division
import numpy as np
import math
import matplotlib.pyplot as plt
import sys

from math import *

def main():

    print 'Number of arguments:', len(sys.argv), 'arguments.'
    print 'Argument List:', str(sys.argv)

    if len(sys.argv) < 1:
        print("Usage expected:\nfilename\n")
        sys.exit()

    #############################################################
    # Get input arguments
    #############################################################
    filename = sys.argv[1]

    ifd=open(filename,"r")
    data=np.fromfile(ifd,dtype=np.int16)
    ifd.close()

    upper16=data[1::2]
    lower16=data[0::2]

    plt.figure(1)
    plt.plot(upper16)
    plt.title("Output Data - Upper 16")
    plt.grid()


    plt.figure(2)
    plt.plot(lower16)
    plt.title("Output Data - Lower 16")
    plt.grid()

    plt.show()

main()

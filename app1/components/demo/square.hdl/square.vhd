-- THIS FILE WAS ORIGINALLY GENERATED ON Wed Jan 11 10:32:01 2017 EST
-- BASED ON THE FILE: square.xml
-- YOU *ARE* EXPECTED TO EDIT IT
-- This file initially contains the architecture skeleton for worker: square

library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all; -- remove this to avoid all ocpi name collisions
architecture rtl of square_worker is
  signal msg_cnt   : unsigned(15 downto 0);
  signal cnt       : unsigned(15 downto 0);
  signal odata_vld : bool_t;
  signal missed_odata_vld : bool_t := '0';
  signal max_sample_cnt   : unsigned(15 downto 0);
begin
  -----------------------------------------------------------------------------
  -- Simple counter to trigger the square pulse ON and OFF
  -----------------------------------------------------------------------------
  counter : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if (ctl_in.reset = '1' or cnt = 63) then
        cnt <= (others => '0');
      elsif (out_in.ready and ctl_in.is_operating) then
        cnt <= cnt + 1;
      end if;
    end if;
  end process counter;
  -----------------------------------------------------------------------------
  -- Generate the square pulse
  -----------------------------------------------------------------------------
  square : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if (ctl_in.reset = '1') then
        out_out.data <= (others => '0');
        odata_vld <= '0';
      elsif (out_in.ready and ctl_in.is_operating) then
        if (cnt < 32) then
          out_out.data <= (others => '1');
        else
          out_out.data <= (others => '0');
        end if;
        odata_vld <= '1';
      else
        odata_vld <= '0';
      end if;
    end if;
  end process square;
  -----------------------------------------------------------------------------
  -- Give (when downstream Worker ready & valid)
  ----------------------------------------------------------------------------- 
  out_out.give <= out_in.ready and (odata_vld or missed_odata_vld) and ctl_in.is_operating;
  -----------------------------------------------------------------------------
  -- Valid Output (when "Give")
  -----------------------------------------------------------------------------
  out_out.valid <= out_in.ready and (odata_vld or missed_odata_vld) and ctl_in.is_operating;
  -----------------------------------------------------------------------------
  -- SOM/EOM - counter set to message size, increment while giving
  -----------------------------------------------------------------------------
  max_sample_cnt <= props_in.messageSize srl 2;

  messageSize_count : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if(ctl_in.reset = '1') then
        msg_cnt <= (others => '0');
      elsif (odata_vld = '1') then
        if(msg_cnt = unsigned(std_logic_vector(max_sample_cnt-1))) then
          msg_cnt <= (others => '0');
        else
          msg_cnt <= msg_cnt + 1;
        end if;
      end if;
    end if;
  end process messageSize_count;

  backPressure : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if(ctl_in.reset = '1' or out_in.ready = '1') then
        missed_odata_vld <= '0';
      elsif (out_in.ready = '0' and odata_vld = '1') then
        missed_odata_vld <= '1';
      end if;
      out_out.byte_enable <= (others => '1');
    end if;
  end process backPressure;

  -----------------------------------------------------------------------------
  -- SOM Output (when downstream Worker ready, valid and message count is zero)
  -----------------------------------------------------------------------------  
  out_out.som <= '1' when (out_in.ready = '1' and odata_vld = '1' and
                           msg_cnt = 0) else '0'; 
  -----------------------------------------------------------------------------
  -- EOM Output (when downstream Worker is ready, valid and
  -- message count is equal to message length - 1)
  -----------------------------------------------------------------------------
  out_out.eom <= '1' when (out_in.ready = '1' and odata_vld = '1' and
                           msg_cnt = max_sample_cnt-1) else '0';

end rtl;

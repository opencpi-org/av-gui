/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package av.proj.ide.assetModel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import av.proj.ide.avps.internal.AvpsResourceManager;

public class EnvironmentProject {
	public EnvironmentProject(String projectName2, String projectPath2) {
		openCpiProjectName = projectName2;
		projectPath = projectPath2;
	}
	
	public String projectPath;
	String openCpiProjectName;
	String eclipseName;
	public String packageId;
	public List<String> dependencies = null;
	
	String projectFolder;
	private boolean registered;
	IProject eclipseProject = null;
	
	public boolean isOpenCpiProject() {
		return openCpiProjectName != null;
	}
	public boolean isOpenInEclipse() {
		return eclipseName != null;
	}
	public String getEclipseName() {
		return eclipseName;
	}
	public String getOpenCpiProjectName() {
		return openCpiProjectName;
	}
	public String getProjectPath() {
		return projectPath;
	}
	public boolean isRegistered() {
		return registered;
	}
	public void setRegistered(boolean registered) {
		this.registered = registered;
	}
	public String getPackageId() {
		return packageId;
	}
	public IProject getEclipseProject() {
		return eclipseProject;
	}
	
	protected String [] showProjectCmd = {"ocpidev", "-d", null, "show", "project", "--local-scope", "--json"};
	
	public void checkForOpenCpiProject() {
		showProjectCmd[2] = projectPath;
		JSONObject jsonObject = getOpenCpiProjectInfo(showProjectCmd);
		
		if(jsonObject == null)return ;
		
		JSONObject projObject = (JSONObject)jsonObject.get("project");
		if(projObject == null) return;
		packageId = (String) projObject.get("package");
		openCpiProjectName = packageId;
	}
	
	public EnvironmentProject (String packageId, JSONObject prjData) {
		this.packageId = packageId;
		openCpiProjectName = packageId;
		String fullPath = (String) prjData.get("real_path");
		projectPath = fullPath;
		String[] pathSegments = fullPath.split("/");
		projectFolder = pathSegments[pathSegments.length -1];
		registered = true;
		
		JSONObject deps = (JSONObject) prjData.get("dependencies");
			if(deps != null) {
			@SuppressWarnings("unchecked")
			Set<String> keys = deps.keySet();
			if(! keys.isEmpty()) {
				dependencies = new ArrayList<String>(keys.size());
				for(String key : keys) {
					String packId = (String) deps.get(key);
					dependencies.add(packId);
				}
			}
		}
	}
	
	public EnvironmentProject(IProject eProject) {
		eclipseName = eProject.getName();
		IPath path = eProject.getLocation();
		projectPath = path.toOSString();
		String [] pathSegs = path.segments();
		projectFolder = pathSegs[pathSegs.length - 1];
		eclipseProject = eProject;
		registered = false;
	}
	
	public void mergeElipseProjectData(IProject eProject) {
		eclipseName = eProject.getName();
		eclipseProject = eProject;
	}

	public static JSONObject getOpenCpiProjectInfo(String [] command) {
		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
		} catch (IOException | InterruptedException e) {
			AvpsResourceManager.getInstance().writeToNoticeConsole(e.toString());
			return null;
		}
		BufferedReader rd = new BufferedReader(new InputStreamReader(p.getInputStream()) );
        JSONParser parser = new JSONParser();
        Object obj = null;
		try {
			obj = parser.parse(rd);
		} catch (IOException | ParseException e) {
			AvpsResourceManager.getInstance().writeToNoticeConsole(e.toString());
			return null;
		}
       return (JSONObject) obj;
	}
	public void addDependency(String string) {
		if(dependencies == null) {
			dependencies = new ArrayList<String>(1);
		}
		dependencies.add(string);
	}

}

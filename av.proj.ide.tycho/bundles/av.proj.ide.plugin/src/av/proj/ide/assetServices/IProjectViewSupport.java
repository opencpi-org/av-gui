/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package av.proj.ide.assetServices;

import java.util.TreeMap;

import av.proj.ide.assetModel.AssetModelData;
import av.proj.ide.wizards.internal.CreateAssetFields;

public interface IProjectViewSupport {
	
	public void registerProjectView(ProjectViewUpdater receiver);
	
	public TreeMap<String, ProjectModelController> getInitialProjects();
	
	public AssetModelData createAsset (CreateAssetFields assetElem, StringBuilder sb);
	public boolean deleteAsset (AssetModelData asset, StringBuilder sb);
	public boolean registerProject(AssetModelData asset, StringBuilder s);
	public boolean unregisterProject(AssetModelData asset, StringBuilder s);

	ProjectModelController getModelController(AssetModelData asset);

}

/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package av.proj.ide.assetServices;

import org.json.simple.JSONObject;

import av.proj.ide.assetModel.AssetModelData;
import av.proj.ide.assetModel.JsonProjectParser;
import av.proj.ide.envServices.EnvBuildTargets;

public class ProjectModelController implements IProjectModelController {

	AssetModelData projectModel;
	
	public AssetModelData getProjectModel() {
		return projectModel;
	}

	ProjectViewUpdater projectView;
	
	public ProjectModelController(AssetModelData projectMdl, ProjectViewUpdater receiver) {
		projectModel = projectMdl;
		projectView = receiver;
	}

	@Override
	synchronized public void populateAsset(AssetModelData asset) {
		asset.clearChildren();
		JsonProjectParser.populateAsset(asset, projectData);
	}
	
	@Override
	public void loadSecondTierData() {
	   	//long start = System.currentTimeMillis();
		Thread loader = new Thread(new Runnable() {
            public void run()
            {
             	String [] project2ndTierCommand = {"ocpidev", "-d", projectModel.getDirectory(), "show", "project", "-v", "--json"};
            	JSONObject projectTiers = EnvBuildTargets.getEnvInfo(project2ndTierCommand);
            	JsonProjectParser.populateSecondTierData(projectTiers, projectModel);
            	if(projectView != null) {
            		projectView.asyncUpdateProject(projectModel);
            	}
            }
        });
		//loader.setName(start + " LoadSecondTier ");
		loader.start();
	}

	@Override
	public void refresh() {
		// TODO Need to finish refresh

	}

	private JSONObject projectData;
	@Override
	public void loadFullProjectData() {
	   	//long start = System.currentTimeMillis();
		Thread loader = new Thread(new Runnable() {
            public void run()
            {
            	getFullProjectData();
            }
        });
		//loader.setName(start + " LoadSecondTier ");
		loader.start();
	}
	
	synchronized public void getFullProjectData() {
    	String [] projectDataCommand = {"ocpidev", "-d", projectModel.getDirectory(), "show", "project", "-vv", "--json"};
    	projectData = EnvBuildTargets.getEnvInfo(projectDataCommand);
	}
}

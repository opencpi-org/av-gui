/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package av.proj.ide.assetServices;

import java.util.TreeMap;

import av.proj.ide.assetModel.AssetModelData;
import av.proj.ide.assetModel.OpenCPICategory;
import av.proj.ide.envServices.OpenCpiEnvironmentService;
import av.proj.ide.wizards.internal.CreateAssetFields;

public class ProjectViewSupport implements IProjectViewSupport {
	private static ProjectViewSupport instance = null;
	public static ProjectViewSupport getInstance() {
		if(instance == null) {
			instance = new ProjectViewSupport();
		}
		return instance;
	}
	private ProjectEnvironmentLoader projectsData;
	
	@Override
	public void registerProjectView(ProjectViewUpdater view) {
		projectsData = new ProjectEnvironmentLoader(view);
		projectView = view;
	}
	private ProjectViewUpdater projectView;
	private ProjectViewSupport() {
	}
	
	TreeMap<String, ProjectModelController> projects;
	
	@Override
	/***
	 * This will block into the projects are obtained.
	 */
	public TreeMap<String, ProjectModelController> getInitialProjects() {
		projects = projectsData.getInitialProjects();
		return projects;
	}

	@Override
	public boolean deleteAsset(AssetModelData asset, StringBuilder sb) {
		boolean r = true; // OpenCpiEnvironmentService.getInstance().deleteAsset(asset, sb);
		if(r) {
			projectView.removeAsset(asset);
		}
		return false;
	}

	@Override
	public boolean registerProject(AssetModelData project, StringBuilder s) {
		String packageId = project.projectLocation.getPackageId();
		// 1. Execute the command
		boolean r = OpenCpiEnvironmentService.getInstance().registerProject(project, s);
		if(! packageId.equals(project.projectLocation.getPackageId())) {
			// the Project's display name has changed;
			projectView.changeProjectName(project);
		}
		return r;
	}
	@Override
	public boolean unregisterProject(AssetModelData project, StringBuilder s) {
		return OpenCpiEnvironmentService.getInstance().unregisterProject(project, s);
	}
	@Override
	public AssetModelData createAsset(CreateAssetFields assetElem, StringBuilder sb) {
		OpenCpiEnvironmentService srv = OpenCpiEnvironmentService.getInstance();
		AssetModelData asset = srv.createAsset(assetElem, sb);
		if(asset == null) {
			//put up the dialog
		}
		else {
			if(assetElem.getType() == OpenCPICategory.project) {
				ProjectModelController modelControl = new ProjectModelController(asset, projectView);
				// TODO: put that in the service: This is a problem because we don't have a package it yet, nor do we have the eclipse project.
				projectsData.openCpiProjects.put(asset.projectLocation.getPackageId(), modelControl);
			}
			return asset;
		}
		return null;
	}
	@Override
	public ProjectModelController getModelController(AssetModelData asset) {
		String project = asset.projectLocation.packageId;
		return projects.get(project);
	}

}

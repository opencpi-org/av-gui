package av.proj.ide.envServices;

import java.util.List;

import av.proj.ide.envServices.EnvBuildTargets.HdlPlatformInfo;
import av.proj.ide.envServices.EnvBuildTargets.RccPlatformInfo;

public class BuildEnvDifferences {
	List<HdlPlatformInfo> hdlAddList;
	List<HdlPlatformInfo> hdlRemoveList;
	Boolean add_remove;
	List<RccPlatformInfo> rccAddList;
	List<RccPlatformInfo> rccRemoveList;
	
	boolean areBuildPlatformsDifferent() {
		boolean areDifferent = true;
		if(hdlAddList.isEmpty() && hdlRemoveList.isEmpty() &&
			rccAddList.isEmpty() && rccRemoveList.isEmpty()) {
			areDifferent = false;
		}
		
		return areDifferent;
	}

}

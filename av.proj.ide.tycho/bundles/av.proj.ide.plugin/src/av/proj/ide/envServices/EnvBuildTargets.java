/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package av.proj.ide.envServices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import av.proj.ide.assetServices.SyncLock;
import av.proj.ide.avps.internal.AvpsResourceManager;

/**
 * Here are classes used as the target and platform data model
 * as well as routines to obtained and parse the JSON objects.
 */
public class EnvBuildTargets {
	
	TreeMap<String, HdlVendor> buildEnvironment;
	TreeMap<String, HdlPlatformInfo> hdlPlatformList;
	TreeMap<String, RccPlatformInfo> rccPlatformList;
	private SyncLock lock = new SyncLock();
	public EnvBuildTargets() {
		lock.acquire();
		loadBuildTargets();
		
	}
	public class RccPlatformInfo {
		String name;
		String target;
		
		public RccPlatformInfo(String name, JSONObject platform) {
			this.name = name;
			target = (String) platform.get("target");
		}

		public String getName() {
			return name;
		}

		public String getTarget() {
			return target;
		}
	}
	
	public class HdlTargetInfo {
		String name;
		String vendor;
		String toolSet;
		protected Collection<String> parts;
		
		public HdlTargetInfo(String name, String vendorName, JSONObject target) {
			this.name = name;
			vendor = vendorName;
			toolSet = null;
			parts = new TreeSet<String>();
			toolSet = (String)target.get("tool");
			JSONArray prts = (JSONArray) target.get("parts");
			if(prts == null)
				return;
			
			for (int i = 0; i < prts.size(); i++) {
				String part = (String) prts.get(i);
				parts.add(part);
			}
		}
		
		public Collection<String> getParts() {
			return parts;
		}
		
	}
	
	public class HdlPlatformInfo extends HdlTargetInfo {
		String part;
		String target;
		boolean built;
		
		public HdlPlatformInfo(String name, JSONObject platform) {
			super(name, (String) platform.get("vendor"), platform);
			part = (String) platform.get("part");
			target = (String) platform.get("target");
			built = (Boolean) platform.get("built");
		}

		public String getName() {
			return name;
		}

		public String getVendor() {
			return vendor;
		}

		public String getToolSet() {
			return toolSet;
		}

		public String getPart() {
			return part;
		}

		public String getTarget() {
			return target;
		}

		public boolean isBuilt() {
			return built;
		}
		
		public int hashCode() {
			return name.hashCode();
		}
		
		public boolean equals(Object o) {
			if(o instanceof HdlPlatformInfo) {
				
				return name.equals(((HdlPlatformInfo)o).name);
			}
			return false;
		}
	}
	
	public class HdlVendor {
		String vendor;
		TreeMap<String, HdlTargetInfo> targetList;
		TreeMap<String, HdlPlatformInfo> platformList;
		
		public HdlVendor(String name, JSONObject vendor) {
			this.vendor = name;
			targetList = new TreeMap<String, HdlTargetInfo>();
			
	        @SuppressWarnings("unchecked")
			Set<String> targetNames = (Set<String>)vendor.keySet();
			if(targetNames == null)
				return;

			for (String tname : targetNames) {
				JSONObject target = (JSONObject) vendor.get(tname);
				HdlTargetInfo targ = new HdlTargetInfo(tname, name, target);
				targetList.put(tname, targ);
			}
		}

		void loadPlatforms(Collection<HdlPlatformInfo> platforms) {
			platformList = new TreeMap<String, HdlPlatformInfo>();
			for(HdlPlatformInfo platform : platforms) {
				if(platform.vendor.equals(this.vendor)) {
					platformList.put(platform.name, platform);
				}
			}
		}
		
		public String getVendor() {
			return vendor;
		}

//		public Collection <HdlTargetInfo> getTargetList() {
//			return targetList.values();
//		}

		public Collection<String> getTargets() {
			return targetList.keySet();
		}
//		public Collection <HdlPlatformInfo> getPlatformList() {
//			return platformList.values();
//		}

//		public Collection<String> getPlatforms() {
//			return platformList.keySet();
//		}
		
	}

	public Collection<HdlVendor> getVendors () {
		lock.acquire();
		lock.release();
		return buildEnvironment.values();
	}
	public Collection<HdlPlatformInfo> getHdlPlatforms() {
		lock.acquire();
		lock.release();
		return hdlPlatformList.values();
	}
	public Collection<RccPlatformInfo> getRccPlatforms() {
		lock.acquire();
		lock.release();
		return rccPlatformList.values();
	}
	public BuildEnvDifferences getPlatformsAdded(EnvBuildTargets otherEnv) {
		BuildEnvDifferences diffs = new BuildEnvDifferences();
		diffs.loadPlatformsAdded(otherEnv);
		return diffs;
	}
	public BuildEnvDifferences getPlatformsRemoved(EnvBuildTargets otherEnv) {
		BuildEnvDifferences diffs = new BuildEnvDifferences();
		diffs.loadPlatformsRemoved(otherEnv);
		return diffs;
	}
	
	public void loadVendorPlatforms () {
		for(HdlVendor vendor : buildEnvironment.values()) {
			vendor.loadPlatforms(hdlPlatformList.values());
		}
	}
	
	void buildHdlVendors() {
		buildEnvironment = new TreeMap<String, HdlVendor> ();
		
		JSONObject jsonObject = getEnvInfo(hdlTargetsCmd);		
 		if(jsonObject == null) {
			AvpsResourceManager.getInstance().writeToNoticeConsole("Null JSON object returned from the environment. Something is wrong with ocpidev show hdl targets.");
		}

        @SuppressWarnings("unchecked")
		Set<String> keys = jsonObject.keySet();
		if(keys == null)
			return;
        
        for(String key : keys) {
        	 JSONObject vendorObj = (JSONObject) jsonObject.get(key);
        	 HdlVendor vendor = new HdlVendor(key, vendorObj);
        	 buildEnvironment.put(key, vendor);
        }
	}
	
	void buildHdPlatforms() {
		hdlPlatformList = new TreeMap<String, HdlPlatformInfo>();
		JSONObject jsonObject = getEnvInfo(hdlPlatformsCmd);		
 
        if(jsonObject == null) {
			AvpsResourceManager.getInstance().writeToNoticeConsole("Null JSON object returned from the environment. Something is wrong with ocpidev show hdl platforms.");
			return;
        }

        @SuppressWarnings("unchecked")
		Set<String> keys = jsonObject.keySet();
        for(String key : keys) {
        	 JSONObject platformObj = (JSONObject) jsonObject.get(key);
        	 HdlPlatformInfo platform = new HdlPlatformInfo(key, platformObj);
        	 hdlPlatformList.put(key,platform);
        }
	}
	
	void buildRccPlatforms() {
		rccPlatformList = new TreeMap<String, RccPlatformInfo>();
		JSONObject jsonObject = getEnvInfo(rccPlatformsCmd);		
		if(jsonObject == null) {
			AvpsResourceManager.getInstance().writeToNoticeConsole("Null JSON object returned from the environment. Something is wrong with ocpidev show rcc platforms.");
			return;
		}

		@SuppressWarnings("unchecked")
		Set<String> keys = jsonObject.keySet();
		if(keys == null)
			return;
        
        for(String key : keys) {
        	 JSONObject platformObj = (JSONObject) jsonObject.get(key);
        	 RccPlatformInfo platform = new RccPlatformInfo(key, platformObj);
        	 rccPlatformList.put(key, platform);
        }
	}
	
	protected String [] hdlTargetsCmd = {"ocpidev", "show", "hdl", "targets", "--json"};
	protected String [] hdlPlatformsCmd = {"ocpidev", "show", "hdl", "platforms", "--json"};
	protected String [] rccPlatformsCmd = {"ocpidev", "show", "rcc", "platforms", "--json"};
		 
	
	public static JSONObject getEnvInfo(String [] command) {
		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
		} catch (IOException | InterruptedException e) {
			AvpsResourceManager.getInstance().writeToNoticeConsole(e.toString());
			return null;
		}
		BufferedReader rd = new BufferedReader(new InputStreamReader(p.getInputStream()) );
        JSONParser parser = new JSONParser();
        Object obj = null;
		try {
			obj = parser.parse(rd);
		} catch (IOException | ParseException e) {
			AvpsResourceManager.getInstance().writeToNoticeConsole(e.toString());
			return null;
		}
       return (JSONObject) obj;
	}
	
	private ArrayList<Thread> loadThreads = new ArrayList<Thread>();
	protected void loadBuildTargets() {
     	//long start = System.currentTimeMillis();
		Thread loader = new Thread(new Runnable() {
            public void run()
            {
            	getBuildTargets();
            }
        });
		//loader.setName(start + " Start HdlPlatforms ");
		loader.start();
	}
	
	protected void getBuildTargets() {
     	long start = System.currentTimeMillis();
		Thread loader = new Thread(new Runnable() {
            public void run()
            {
            	buildHdPlatforms();
            }
        });
		loadThreads.add(loader);
		loader.setName(start + " Start HdlPlatforms ");
		loader.start();
		
		loader = new Thread(new Runnable() {
            public void run()
            {
            	buildHdlVendors();
            }
        });
		loadThreads.add(loader);
		loader.setName(start + " Start HdlVendors ");
		loader.start();
		
		loader = new Thread(new Runnable() {
            public void run()
            {
            	buildRccPlatforms();
            }
        });
		loadThreads.add(loader);
		loader.setName(start + " Start RCC Platforms ");
		loader.start();
		waitOnThreads();
		lock.release();
	}
	protected void waitOnThreads() {
        boolean done = false;
        while (!done)
        {
            try
            {
        		for(Thread loading : loadThreads) {
        			loading.join();
        			//System.out.println(loading.getName() + " finished " +System.currentTimeMillis());
        		}
                done = true;
                loadThreads.clear();
            }
            catch (InterruptedException e)
            {
    			//System.out.println("Got Interrupt");
            }
        }
        // Perform Second check.
		for(Thread loading : loadThreads) {
			try {
				loading.join();
			} catch (InterruptedException e) {
			}
			//System.out.println(loading.getName() + " finished " +System.currentTimeMillis());
		}
        
	}

	
	public class BuildEnvDifferences {
		List<HdlPlatformInfo> hdlAddList = null;
		List<HdlPlatformInfo> hdlRemoveList = null;;
		Boolean add_remove;
		List<RccPlatformInfo> rccAddList = null;
		List<RccPlatformInfo> rccRemoveList = null;
		
		boolean arePlatformsAdded() {
			if(hdlAddList.isEmpty() && rccAddList.isEmpty()) {
				return false;
			}
			return true;
		}
		boolean arePlatformsRemoved() {
			if(hdlRemoveList.isEmpty() && rccRemoveList.isEmpty()) {
				return false;
			}
			return true;
		}
		List<HdlPlatformInfo> getHdlPlaformsAdded() {
			return hdlAddList;
		}
		List<RccPlatformInfo> getRccPlaformsAdded() {
			return rccAddList;
		}
		List<HdlPlatformInfo> getHdlPlaformsRemoved() {
			return hdlRemoveList;
		}
		List<RccPlatformInfo> getRccPlaformsRemoved() {
			return rccRemoveList;
		}

		public void loadPlatformsRemoved(EnvBuildTargets otherEnv) {
			hdlRemoveList = new ArrayList<HdlPlatformInfo>();
			rccRemoveList = new ArrayList<RccPlatformInfo>();
			//Hack
			otherEnv.getHdlPlatforms();
			Collection<String> keys = hdlPlatformList.keySet();
			for(String key : keys) {
				if(! otherEnv.hdlPlatformList.containsKey(key)) {
					hdlRemoveList.add(hdlPlatformList.get(key));
				}
			}
			keys = otherEnv.rccPlatformList.keySet();
			for(String key : keys) {
				if(! rccPlatformList.containsKey(key)) {
					rccRemoveList.add(otherEnv.rccPlatformList.get(key));
				}
			}
			
		}

		public void loadPlatformsAdded(EnvBuildTargets otherEnv) {
			hdlAddList = new ArrayList<HdlPlatformInfo>();
			rccAddList = new ArrayList<RccPlatformInfo>();
			otherEnv.getHdlPlatforms();
			Collection<String> keys = otherEnv.hdlPlatformList.keySet();
			for(String key : keys) {
				if(! hdlPlatformList.containsKey(key)) {
					hdlAddList.add(otherEnv.hdlPlatformList.get(key));
				}
			}
			keys = otherEnv.rccPlatformList.keySet();
			for(String key : keys) {
				if(! rccPlatformList.containsKey(key)) {
					rccAddList.add(otherEnv.rccPlatformList.get(key));
				}
			}
		}
	}
}

/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package av.proj.ide.envServices;

import java.util.Collection;
import java.util.Map;

import av.proj.ide.assetModel.EnvironmentProject;
import av.proj.ide.internal.UiComponentSpec;

public interface IEnvironmentInfoService {

	// ==================================================================
	//  AngryViper Wizard Support
	public Map<String, EnvironmentProject> getRegisteredProjectsLessCore();
	public Map<String, EnvironmentProject> getOpenCpiWorkspaceProjects();
	public EnvironmentProject getProjectByEclipseName(String projectName);
	
	// Library and Component Information
	public Collection<String> getProjectComponentLibraries(String projectId);
	public Map<String, UiComponentSpec> getComponentsInLibrary(String projectId, String libraryId);	
	public Map<String, UiComponentSpec> getComponentsAvailableToProject(String projectId);
	public String getComponentName(String wizardName);
	//======================================================================
	
	// OAS Support
	public Map<String, UiComponentSpec> getApplicationComponents();
	public UiComponentSpec getUiSpecByDisplayName(String projectDisplayName);
	public UiComponentSpec getUiSpecByFileInfo(String fullLibraryPath, String specFileName, String specFilePathname);
	
	// OCS Support
	Collection<String> getProtocols();
	
	// OHAD Support
	Collection<String> getHdlWorkers();

}

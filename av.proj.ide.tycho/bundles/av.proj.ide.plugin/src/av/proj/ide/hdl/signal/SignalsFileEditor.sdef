<?xml version="1.0" encoding="UTF-8"?>
<!--
   - This file is protected by Copyright. Please refer to the COPYRIGHT file
   - distributed with this source distribution.
   -
   - This file is part of OpenCPI <http://www.opencpi.org>
   -
   - OpenCPI is free software: you can redistribute it and/or modify it under
   - the terms of the GNU Lesser General Public License as published by the Free
   - Software Foundation, either version 3 of the License, or (at your option)
   - any later version.
   -
   - OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   - WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   - FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   - more details.
   -
   - You should have received a copy of the GNU Lesser General Public License
   - along with this program. If not, see <http://www.gnu.org/licenses/>.
-->

<definition>
    <editor-page>
        <id>SignalsFileEditorPage</id>
        <page-header-text>Signals File Editor</page-header-text>
        <initial-selection>Signals</initial-selection>
        <element-type>av.proj.ide.hdl.signal.Signals</element-type>
        <root-node>
            <node>
                <label>Signals</label>
                
                <section>
                    <content>
                        <actuator>
                            <action-id>Sapphire.Add</action-id>
                            <label>New Signal</label>
                        </actuator>
                    </content>
                    <description>The signals file bundles signal(s) related to a common interface. Typically these are wires within an FPGA design that are connected between device workers (via a devsignal port).</description>
                    <label>HDL Signals</label>
                </section>
                
                <node-factory>
                    <property>Signals</property>
                    <case>
                        <element-type>av.proj.ide.hdl.signal.DeviceSignal</element-type>
                        <section>
                            <label>Signal Details</label>
                            <content>
                                <include>DeviceSignalsForm</include>
                            </content>
                            <scroll-vertically>true</scroll-vertically>
                        </section>
                        <label>${ Name == null ? "new_name" : Name }</label>
                    </case>
                </node-factory>
                
            </node>
        </root-node>
    </editor-page>
    
    <import>
        <definition>av.proj.ide.hdl.signal.DeviceSignalsForm</definition>
    </import>
    
</definition>
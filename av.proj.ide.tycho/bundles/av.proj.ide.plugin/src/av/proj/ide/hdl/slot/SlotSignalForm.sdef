<?xml version="1.0" encoding="UTF-8"?>
<!--
   - This file is protected by Copyright. Please refer to the COPYRIGHT file
   - distributed with this source distribution.
   -
   - This file is part of OpenCPI <http://www.opencpi.org>
   -
   - OpenCPI is free software: you can redistribute it and/or modify it under
   - the terms of the GNU Lesser General Public License as published by the Free
   - Software Foundation, either version 3 of the License, or (at your option)
   - any later version.
   -
   - OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   - WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   - FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   - more details.
   -
   - You should have received a copy of the GNU Lesser General Public License
   - along with this program. If not, see <http://www.gnu.org/licenses/>.
-->

<definition>

    <form>
        <id>SignalCharacteristicsForm</id>
        <content>
            <property-editor>Name</property-editor>
            <property-editor>
                <hint>
                    <name>checkbox.layout</name>
                    <value>leading.label</value>
                </hint>
                <property>Direction</property>
                <style>Sapphire.PropertyEditor.RadioButtonGroup</style>
            </property-editor>

            <section>
               <content>
	            <label>- Name: It is recommended the signal name be consistent with the pin naming in the datasheet or specification, e.g. VITA 57.1 FMC.</label>
	            <label>- Direction: Signal direction is relative to the FPGA.</label>
                    <label>-- Setting to bidirectional will cause the direction to be inferred from a connected device worker signal.</label>
                    <label>-- Setting to in or out will force the direction to be the same as a connected device worker signal.</label>
                    <label>-- Setting to inout will force a tristate primitive to be instanced in the FPGA design.</label>
               </content>
                <label>Notes on Defining Slot Signals</label>
                <collapsible>true</collapsible>
                <collapsed-initially>true</collapsed-initially>
			</section>
			
            <section>
                <content>
                     <section>
                        <label>Advanced: Override In-Out Naming</label>
                        <description>Note: Use &quot;%s&quot; in the name string to have the signal name in the desired location; e.g. &quot;my%s_in&quot; produces my&lt;signal_name&gt;_in.</description>
                        <collapsible>true</collapsible>
                        <collapsed-initially>true</collapsed-initially>
                        <content>
		                    <property-editor>In</property-editor>
		                    <property-editor>Out</property-editor>
		                    <property-editor>Oe</property-editor>
                        </content>
                    </section>
                </content>
                <label>In-Out Signal Naming Convention</label>
                <visible-when>${ Direction == 'Inout' }</visible-when>
                <description>By default the OpenCPI names for in-out signal names are: &lt;signal_name&gt;_I &lt;signal_name&gt;_O and &lt;signal_name&gt;_OE. Expand the advanced section to name these differently.</description>
            </section>
                    
        </content>
    </form>

</definition>
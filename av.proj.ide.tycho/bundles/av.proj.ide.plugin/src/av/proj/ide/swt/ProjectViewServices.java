/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package av.proj.ide.swt;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.filebuffers.FileBuffers;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ListSelectionDialog;
import org.eclipse.ui.wizards.IWizardDescriptor;

import av.proj.ide.assetModel.AngryViperFileService;
import av.proj.ide.assetModel.AssetModelData;
import av.proj.ide.assetModel.OpenCPICategory;
import av.proj.ide.assetServices.ProjectViewSupport;
import av.proj.ide.avps.internal.AvpsResourceManager;
import av.proj.ide.wizards.NewOcpiAssetWizard;

public class ProjectViewServices {

	public void openEditor(AssetModelData asset, Composite view) {
		
//		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
//		IProject project = root.getProject(asset.projectLocation.getEclipseName());
//		IFolder folder = AngryViperFileService.getAssetFolder(asset, project);
//		if(folder == null || ! folder.exists()) {
//			AvpsResourceManager.getInstance()
//			.writeToNoticeConsole("Unable to find this asset's parent folder. Use Project Explorer to look for it.");
//			return;
//		}
		StringBuilder sb = new StringBuilder();
		File[] xmlFiles = AngryViperFileService.getAssetFile(asset, sb);
		if(xmlFiles == null) {
			AvpsResourceManager.getInstance().writeToNoticeConsole(sb.toString());
			return;
		}
		else if(xmlFiles.length == 1) {
			File file = xmlFiles[0];
			IFile wbFile = FileBuffers.getWorkspaceFileAtLocation(Path.fromOSString(file.getPath()));
			openThisEditor(asset, wbFile, view);
			return;
		}
		String[] names = new String[xmlFiles.length];
		int i = 0;
		for(File file : xmlFiles) {
			names[i] = file.getName();
			i++;
		}
		ListSelectionDialog dialog = 
		new ListSelectionDialog(view.getShell(), xmlFiles, ArrayContentProvider.getInstance(),
				fileLabel, "Open one or more of these?");
		dialog.setTitle("Unable to find asset XML file.");
		//dialog.setInitialSelections(names);
		dialog.open();
		Object[] result = dialog.getResult();
		if(result != null) {
			for(Object name : result) {
				File zname = (File) name;
				IFile wbFile = FileBuffers.getWorkspaceFileAtLocation(Path.fromOSString(zname.getPath()));
				if(wbFile.exists()) {
					openThisEditor(asset, wbFile, view);
				}
			}
		}
	}
	
	protected void openThisEditor(AssetModelData asset, IFile assetFile, Composite view) {
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		
		try {
			AngryViperFileService.openEditor(asset, null, assetFile, page, null, false);
		} catch (CoreException e) {
			AvpsResourceManager.getInstance().writeToNoticeConsole("Internal Eclipse runtime error occurred. \n --> " + e.toString() );
		}
	}
	public void unregisterProject(AssetModelData asset, Composite view) {
		StringBuilder sb = new StringBuilder("Remove ");
		sb.append(asset.category.getFrameworkName());
		sb.append(" ");
		sb.append(asset.assetName);
		sb.append(" from the project registry?");

		boolean confirmed = MessageDialog.openConfirm(view.getShell(), "Confirm", 
		sb.toString());
		if (confirmed) {
			StringBuilder s = new StringBuilder();
			boolean r = ProjectViewSupport.getInstance().unregisterProject(asset, s);
			if (!r) {
				MessageDialog.openConfirm(view.getShell(), "OpenCPI Delete Failed", s.toString());
			}
		}
	}

	public void registerProject(AssetModelData asset, Composite view) {
		StringBuilder s = new StringBuilder();
		boolean r = ProjectViewSupport.getInstance().registerProject(asset, s);
		if (!r) {
			MessageDialog.openConfirm(view.getShell(), "OpenCPI Delete Failed", s.toString());
		}
	}

	public void deleteAsset(AssetModelData asset, Composite view) {
		StringBuilder sb = new StringBuilder("Delete ");
		sb.append(asset.assetName);
		if(asset.category != OpenCPICategory.project) {
			sb.append(" in project ");
			sb.append(asset.projectLocation.getPackageId());
		}
		boolean confirmed = MessageDialog.openConfirm(view.getShell(), "Confirm", 
		sb.toString());
		if (confirmed) {
;
			//IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

			IRunnableWithProgress op = new IRunnableWithProgress() {
				@Override
				public void run(IProgressMonitor monitor) {
					StringBuilder s = new StringBuilder();
					boolean r = ProjectViewSupport.getInstance().deleteAsset(asset, s);
					if (!r) {
						MessageDialog.openConfirm(view.getShell(), "OpenCPI Delete Failed", s.toString());
						return;
					}
					
					try {
						IProject project = asset.projectLocation.getEclipseProject();
						if(asset.category == OpenCPICategory.project){
							project.close(monitor);
							project.delete(false, false, monitor);
							//root.refreshLocal(IResource.DEPTH_ONE, monitor);
						}
						else {
							IFile file = project.getFile(asset.assetName);
							if(file.exists()) {
								file.delete(false, monitor);
							}
							else {
								IFolder folder = project.getFolder(asset.assetName);
								if(folder.exists()) {
									folder.delete(false, monitor);
								}
							}
							project.refreshLocal(IResource.DEPTH_INFINITE, monitor);
						}
					} catch (CoreException e) {
						//e.printStackTrace();
					}
				}
			};
			Shell shell = view.getShell();
			try {
				new ProgressMonitorDialog(shell).run(true, true, op);
			} catch (InvocationTargetException | InterruptedException e) {
			}
		}
	}	

	class FileLabelProvider extends LabelProvider {
		
		@Override
		public String getText(Object o) {
			if(o instanceof File) {
				File f = (File) o;
				return f.getName();
			}
			return null;
		}
	}
	private FileLabelProvider fileLabel = new FileLabelProvider();
	
	public void putUpWizard() {
		try {
			NewOcpiAssetWizard wizard = createWizard();
			if(wizard == null) return;
			
			WizardDialog wd = new WizardDialog(Display.getDefault().getActiveShell(), wizard);
			wd.setTitle(wizard.getWindowTitle());
			wd.open();
		} catch (CoreException e) {
			AvpsResourceManager.getInstance().writeToNoticeConsole("Internal Eclipse runtime error occurred. \n --> "
					                         + e.toString() );
		}
	}
	public void putUpSpecificWizard(OpenCPICategory selectedType, AssetModelData asset) {
		try {
			NewOcpiAssetWizard wizard = createWizard();
			if(wizard == null) return;
			wizard.setupSpecificAsset(selectedType, asset);
			
			WizardDialog wd = new WizardDialog(Display.getDefault().getActiveShell(), wizard);
			wd.setTitle(wizard.getWindowTitle());
			wd.open();
		} catch (CoreException e) {
			AvpsResourceManager.getInstance().writeToNoticeConsole("Internal Eclipse runtime error occurred. \n --> "
                    + e.toString() );
		}
	}
	
	NewOcpiAssetWizard createWizard() throws CoreException {
		String id = "av.proj.ide.wizards.NewOcpiAssetWizard";
		IWizardDescriptor descriptor = PlatformUI.getWorkbench().getNewWizardRegistry().findWizard(id);
		if (descriptor == null) {
			descriptor = PlatformUI.getWorkbench().getImportWizardRegistry().findWizard(id);
		}
		Runtime rt = Runtime.getRuntime();
		Process proc;
		
		try {
			proc = rt.exec("ocpidev");
			int exitVal = proc.waitFor();
			if (exitVal != 1) {
				MessageDialog.openError(Display.getDefault().getActiveShell(), "\"ocpidev\" Command Not Found", 
						"Could not locate the \"ocpidev\" command.");
			}
		} catch (IOException | InterruptedException e) {
			MessageDialog.openError(Display.getDefault().getActiveShell(), "\"ocpidev\" Command Not Found", 
					"Could not locate the \"ocpidev\" command.");
			return null;
		}
		NewOcpiAssetWizard wizard = (NewOcpiAssetWizard)descriptor.createWizard();
		return wizard;
	}


}

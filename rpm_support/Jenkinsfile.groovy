import groovy.transform.Field

final TargetOSs = [6,7]

// Which Eclipse? Where?
final Eclipses = [ 'Neon':                  ['file': 'eclipse-cpp-neon-3-linux-gtk-x86_64.tar.gz',
                                             'url' : "https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/neon/3/eclipse-cpp-neon-3-linux-gtk-x86_64.tar.gz&r=1"],
                   'Oxygen - EXPERIMENTAL': ['file': 'eclipse-cpp-oxygen-3a-linux-gtk-x86_64.tar.gz',
                                             'url' : "https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/oxygen/3a/eclipse-cpp-oxygen-3a-linux-gtk-x86_64.tar.gz&r=1"]]

// We repeat these so much, made them consts.
final EclipseAndPrereqs = 'eclipse_with_prereq'
final AVPrereqDir       = 'av_prereqs_download'
final AVPlugin          = 'av.proj.ide.plugin'

// Accessed in closure
@Field final AVEclipse  = 'angryviper_eclipse'
@Field final RPMSupport = 'rpm_support'

properties properties:
[
  parameters
  ([
    choice(       choices: Eclipses.keySet().sort().join("\n"), name: 'Eclipse Version'),
    booleanParam( defaultValue: false, name: 'Clean', description: 'Re-downloads Eclipse and its Prereqs'),
    string(       defaultValue: '', description: 'RPM Release tag (e.g. "beta"). Usually left blank.', name: 'RPM Release')
  ])
]

node('docker-fedora')
{
    println "Job configuration: " + groovy.json.JsonOutput.prettyPrint(groovy.json.JsonOutput.toJson(params))

    final EclipseVersion = params['Eclipse Version']
    echo "User selected ${EclipseVersion}"

    stage('Sync Git')
    {
        // We run the Maven build stage as root, so fix here.
        sh 'sudo chown -R jenkins:jenkins .'

        // Sync/Clean git, but keep our downloads around if we're not doing a clean build.
        final git_info = checkout scm

        def exceptFiles = " -e ${AVPrereqDir} "
        Eclipses.each {
            exceptFiles += " -e ${it.value.file} "
        }

        exceptFiles = params['Clean'] ? "" : exceptFiles

        sh "git clean -fdx ${exceptFiles}"

        dir( RPMSupport )
        {
            // Makefile will use these two files along with others already here, in the RPM stage.
            sh "echo ${git_info.GIT_BRANCH} > jenkins_gitbranch"
            sh "echo ${git_info.GIT_COMMIT} > jenkins_githash"
        }

        sh "tar cf ${RPMSupport}.tar ${RPMSupport}"

        stash includes: "${RPMSupport}.tar", name: "${RPMSupport}.tar-stash"
    }
    stage('Download Eclipse')
    {
        if ( fileExists( Eclipses[EclipseVersion].file ) && !params['Clean'] )
        {
            echo "Reusing ${Eclipses[EclipseVersion].file}"
        }
        else
        {
            sh "rm -f ${Eclipses[EclipseVersion].file}"
            echo "Attempt Download of ${Eclipses[EclipseVersion].file}"
            httpRequest outputFile: Eclipses[EclipseVersion].file, responseHandle: 'NONE', url: Eclipses[EclipseVersion].url
        }
    }
    stage('Eclipse Downloads Prereqs')
    {
        sh "tar xf ${Eclipses[EclipseVersion].file}"

        if ( fileExists( AVPrereqDir ) && !params['Clean'] )
        {
            echo "Reusing ${AVPrereqDir}; assuming it is valid"
        }
        else
        {
            echo "Using Eclipse to download ${AVPrereqDir}"
            dir ( AVPrereqDir )
            {
                deleteDir()
                sh '''${WORKSPACE}/eclipse/eclipse -clean -purgeHistory -application org.eclipse.equinox.p2.director -noSplash -destination ./ \
-repository "http://download.eclipse.org/tools/gef/updates/legacy/releases/, http://download.eclipse.org/releases/neon" \
-installIUs "\
org.eclipse.gef.sdk.feature.group, \
org.eclipse.jdt.feature.group, \
org.eclipse.sapphire.feature.group, \
org.eclipse.sapphire.ui.feature.group, \
org.eclipse.sapphire.platform.feature.group, \
org.eclipse.sapphire.java.jdt.feature.group, \
org.eclipse.sapphire.java.feature.group, \
org.eclipse.sapphire.osgi.feature.group, \
org.eclipse.sapphire.sdk.feature.group, \
org.eclipse.sapphire.ui.swt.gef.feature.group, \
org.eclipse.sapphire.ui.swt.xml.editor.feature.group, \
org.eclipse.sapphire.modeling.xml.feature.group"'''
            }
        }

        dir( 'eclipse/av_prereqs/plugins' )
        {
            deleteDir()
            sh "cp -r ${WORKSPACE}/${AVPrereqDir}/plugins/. ."
            // Delete duplicates
            sh "ls -1 ${WORKSPACE}/eclipse/plugins/ | xargs -r rm -rf"
        }

        sh 'rm -rf eclipse/configuration/*.log'
        sh "tar cf ${EclipseAndPrereqs}.tar eclipse"

        stash includes: "${EclipseAndPrereqs}.tar", name: "${EclipseAndPrereqs}-stash"
    }
    stage('Build Our Plugin')
    {
        echo 'Reusing Fedora base image.'
        def base_image = docker.image('jenkins/av_idebuild:v1')

        base_image.inside('-u 0:0')
        {
            dir( 'av.proj.ide.tycho' )
            {
                sh 'xmvn -o clean verify'
            }
            dir ('av.proj.ide.tycho/releng/av.proj.ide.update/target/repository/plugins/')
            {
                archiveArtifacts "${AVPlugin}_*.jar"
            }
        }
    }
    stage('Insert Plugin -> Eclipse')
    {
        dir('combiner')
        {
            deleteDir()

            unstash name: "${EclipseAndPrereqs}-stash"
            sh "tar xf ${EclipseAndPrereqs}.tar"
            sh 'mkdir -p eclipse/dropins/angryviper/plugins'

            // Theirs
            sh 'mv eclipse/av_prereqs/plugins/* eclipse/dropins/angryviper/plugins/'
            sh 'rm -rf eclipse/av_prereqs'

            // Ours
            unarchive mapping: ["${AVPlugin}_*.jar": '.']
            sh "mv ${AVPlugin}*.jar eclipse/dropins/angryviper/plugins/"

            sh "tar cf ${AVEclipse}.tar eclipse"

            stash includes: "${AVEclipse}.tar", name: "${AVEclipse}-stash"
        }
    }
}

stage('Make RPMs')
{
    stepsForParallel = ['failFast':true]

    TargetOSs.each
    {
        os -> stepsForParallel["CentOS ${os}: IDE Build"] = buildIDECentos( os )
    }

    parallel stepsForParallel
}

// Returns a closure that will build the main RPMs for a given OS!
def buildIDECentos( in_OS )
{
    return {
        def OS = in_OS
        node ("rpmbuild && docker-C${OS}")
        {
            docker.image("jenkins/ocpibuild:v3-C${OS}").inside()
            {
                dir( 'temp_build_area' )
                {
                    deleteDir()

                    unstash name: "${RPMSupport}.tar-stash"
                    sh "tar xf ${RPMSupport}.tar"

                    dir( RPMSupport )
                    {
                        unstash name: "${AVEclipse}-stash"

                        // Temp filename is required by our Makefile
                        sh "mv ${AVEclipse}.tar angryviper_eclipse_temp.tar"
                        
                        // Only pass RPM_RELEASE if it has a value, to play
                        // nice with makefile's use of RPM_RELEASE?=...
                        final RPM_RELEASE = params["RPM Release"].trim()
                        final env_vars = RPM_RELEASE.length() > 0 ? "RPM_RELEASE=${RPM_RELEASE}" : ""
                        
                        sh "make ide ${env_vars}"
                        archiveArtifacts '**/*.rpm'
                    }
                }
            }
        }
    }
}
